use rustyline::hint::{Hinter, HistoryHinter};
use rustyline::{Context};
use rustyline_derive::{Completer, Helper, Highlighter, Validator};

#[derive(Completer, Helper, Validator, Highlighter)]
pub struct Helper {
    hinter: HistoryHinter,
}

impl Hinter for Helper {
    fn hint(&self, line: &str, pos: usize, ctx: &Context<'_>) -> Option<String> {
        self.hinter.hint(line, pos, ctx)
    }
}

pub fn get_helper() -> Helper {
    return Helper {
        hinter: HistoryHinter {},
    };
}
