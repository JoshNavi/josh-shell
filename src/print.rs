use std::io::{self, Write};
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

fn print_with_args(color: Color, content: &str, newline: bool) -> io::Result<()> {
    let mut stdout = StandardStream::stdout(ColorChoice::Always);

    stdout.set_color(ColorSpec::new().set_fg(Some(color)))?;

    if newline {
        writeln!(&mut stdout, "{}", content)?;
    } else {
        write!(&mut stdout, "{}", content)?;
    }

    stdout.reset()?;

    stdout.flush()?;

    Ok(())
}

pub fn normal(content: &str) {
    print_with_args(Color::White, content, false).unwrap();
}

pub fn normalln(content: &str) {
    print_with_args(Color::White, content, true).unwrap();
}

pub fn good(content: &str) {
    print_with_args(Color::Green, content, false).unwrap();
}

pub fn goodln(content: &str) {
    print_with_args(Color::Green, content, true).unwrap();
}

pub fn bad(content: &str) {
    print_with_args(Color::Red, content, false).unwrap();
}

pub fn badln(content: &str) {
    print_with_args(Color::Red, content, true).unwrap();
}

pub fn warnln(content: &str) {
    print_with_args(Color::Yellow, content, true).unwrap();
}
