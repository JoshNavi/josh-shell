use std::env;
use std::path::Path;

use super::status::Status;
use super::super::print;

pub fn handle(args: &[&str]) -> Status {
    if args.len() > 1 {
        print::badln("Too many args for cd command");
        return Status::New(1);
    }

    let home = env::var("HOME").unwrap();

    let new_path = if args.len() == 1 { args[0] } else { home.as_str() };

    let root = Path::new(new_path);

    if let Err(e) = env::set_current_dir(&root) {
        eprintln!("cd: {}", e);
        return Status::New(1);
    }

    return Status::New(0);
}
