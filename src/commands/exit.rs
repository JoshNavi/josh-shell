use super::status::Status;

static GOODBYE: &str = "Bye!";

pub fn handle() -> Status {
    println!("{}", GOODBYE);
    return Status::Done;
}
