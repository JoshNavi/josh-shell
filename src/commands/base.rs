use std::process::Command;
use super::status::Status;

pub fn handle(command: &str, args: &[&str]) -> Status {
    let child = Command::new(command).args(args).spawn();

    let process = match child {
        Ok(mut child) => child.wait(),
        Err(e) => {
            eprintln!("jsh: Command not found: {}", command);
            eprintln!("Original Error: {}", e);
            return Status::New(1);
        },
    };

    let result = match process {
        Ok(process) => Status::New(process.code().unwrap()),
        Err(_) => {
            eprintln!("Command {} was never running", command);
            return Status::New(1);
        },
    };


    return result;
}
