#[derive(Copy, Clone, Debug)]
pub enum Status {
    New(i32),
    Previous,
    Done,
}
