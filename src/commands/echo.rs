use super::status::Status;

pub fn handle(args: &[&str]) -> Status {
    println!("{}", args.join(" "));
    return Status::New(0);
}
