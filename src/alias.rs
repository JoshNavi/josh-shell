use std::collections::HashMap;

pub fn load() -> HashMap<&'static str, &'static str> {
   let mut aliases = HashMap::new();

   aliases.insert("ls", "exa -laa");
   aliases.insert("grep", "rg");

   return aliases;
}
