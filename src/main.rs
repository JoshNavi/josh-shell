use std::collections::HashMap;
use std::env;
use rustyline::error::ReadlineError;
use rustyline::Editor;
use dirs;

mod commands;
mod print;
mod alias;
mod helper;

use commands::status::Status;

static GREETING: &str = "Welcome to jsh!";
static PROMPT: &str = "~~> ";

fn main() {
    let aliases = alias::load();

    print::normalln(GREETING);
    print::normalln("");

    let mut rl = Editor::new();

    rl.set_helper(Some(helper::get_helper()));

    if rl.load_history("history").is_err() {
        print::warnln("No previous history");
    }

    let mut _previous_status = Status::New(0);

    loop {
        prompt_dir();

        // TODO: Replace readline with a lib that doesnt suck
        let readline = rl.readline(PROMPT);

        match readline {
            Ok(line) => {
                let as_str = line.as_str();
                rl.add_history_entry(as_str);
                let new_status = handle(&aliases, as_str);
                match new_status {
                    Status::New(_) => _previous_status = new_status,
                    Status::Done => { break; }
                    _ => (),
                };
            },
            Err(ReadlineError::Interrupted) => {
                println!("CTRL-C");
                break;
            },
            Err(ReadlineError::Eof) => {
                println!("CTRL-D");
                break;
            },
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            },
        };

        println!("");
    }

    rl.save_history("history").unwrap();
}

fn _prompt(status: Status) {
    match status {
        Status::New(x) if x == 0 => print::good(PROMPT),
        _ => print::bad(PROMPT),
    };
}

fn prompt_dir() {
    let home_path = dirs::home_dir().unwrap();
    let home_path_as_str = home_path.to_str().unwrap();

    let path = env::current_dir().unwrap();
    let path_as_str = path.as_path().to_str().unwrap();

    let formatted = path_as_str.replace(home_path_as_str, "~");
    print::normal(formatted.as_str())
}

fn handle(aliases: &HashMap<&str, &str>, raw: &str) -> Status {
    let input: Vec<&str> = raw.trim().split(" ").collect();

    let status = match input.as_slice() {
        ["exit", ..] | ["quit", ..] => commands::exit::handle(),
        ["echo", args @ ..] => commands::echo::handle(args),
        ["cd", args @ ..] => commands::cd::handle(args),
        ["", ..] => commands::noop::handle(),
        [command, args @ ..] => {
            return match aliases.get(command) {
                // Hack for making sure we dont run into recursive alises
                Some(alias) => handle(&HashMap::new(), &raw.replace(command, alias)),
                None => commands::base::handle(command, args),
            };
        },
        _ => commands::noop::handle(),
    };

    return status;
}
