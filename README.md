# jsh

The Josh Shell, my personal shell. A work in progress (obviously)

## Design

IDK yet.

### What makes this shell different from all the other shells?

Not much.

## Completed

- [x] cd (basic)
- [x] echo (basic)

## TODO

- [ ] Abbreviations
- [ ] Aliases
- [ ] Parameter expansion
- [ ] Completions
- [ ] Functions
- [ ] Pipes
- [ ] Redirection
- [ ] Chaining commands
  - [ ] `;`
  - [ ] `&&`
  - [ ] `||`
